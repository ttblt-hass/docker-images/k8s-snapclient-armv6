IMAGE=registry.gitlab.com/ttblt-hass/docker-images/k8s-snapclient-armv6
TAG=0.19.0-3

docker:
	docker build --build-arg SNAP_VERSION=0.19.0 -t $(IMAGE):$(TAG) .

push:
	docker push $(IMAGE):$(TAG)

run:
	docker run --rm --privileged -e SNAP_SERVER=192.168.0.1 -e AUDIO_CARD=sndrpihifiberry -e HOSTID=test  $(IMAGE):$(TAG)
