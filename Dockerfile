FROM arm32v6/alpine:edge

ENV SNAP_SERVER=""
ENV AUDIO_CARD=sndrpihifiberry
ENV HOSTID=""

# https://pkgs.alpinelinux.org/package/edge/community/armhf/snapcast-client
RUN apk add --no-cache snapcast-client --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community

CMD /usr/bin/snapclient -h ${SNAP_SERVER} -s ${AUDIO_CARD} --hostID ${HOSTID}
